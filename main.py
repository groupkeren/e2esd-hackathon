import os
import telebot
from telebot import types

secret_key = os.environ['API_KEY']
bot = telebot.TeleBot(secret_key)


@bot.message_handler(commands=['Greet'])
def greet(message):
  bot.reply_to(message, "Hey! Hows it going?")

@bot.message_handler(commands=['hello'])
def hello(message):
  bot.send_message(message.chat.id, "Hello!")
  
@bot.inline_handler(lambda query: query.query == 'test')
def test(inline_query):
  try:
    r = types.InlineQueryResultArticle('1', 'test', types.InputTextMessageContent('Hello!'))
    r2 = types.InlineQueryResultArticle('2', 'test2', types.InputTextMessageContent('Greetings!'))
    bot.answer_inline_query(inline_query.id, [r, r2])
  except Exception as e: 
    print(e)
  
bot.polling(True)